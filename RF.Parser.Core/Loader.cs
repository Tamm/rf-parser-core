﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using ParserLibrary.Common;
using ParserLibrary.Structs;

namespace RF.Parser.Core
{
    public class Loader
    {
        // Server store
        public ServerStore serverStore;

        // Client store
        public ClientStore clientStore;

        // Armor server
        public EquipmentItem helmetItem;
        public EquipmentItem upperItem;
        public EquipmentItem lowerItem;
        public EquipmentItem gauntletItem;
        public EquipmentItem shoeItem;

        // Armor client
        public ArmorItemClient helmetItemClt;
        public ArmorItemClient upperItemClt;
        public ArmorItemClient lowerItemClt;
        public ArmorItemClient gauntletItemClt;
        public ArmorItemClient shoeItemClt;
        public ArmorItemClient shieldItemClt;

        // Accessories client
        public AccessoryItemClient amuletItemClt;
        public AccessoryItemClient ringItemClt;

        // Weapon client
        public WeaponItemClient weaponItemClt;

        private ClientDataHeader ReadClientDataHeader(BinaryReader br)
        {
            ClientDataHeader header = new ClientDataHeader
            {
                nBlocks = br.ReadUInt32(),
                nColumns = br.ReadUInt32()
            };

            return header;
        }

        private ServerDataHeader ReadServerDataHeader(BinaryReader br)
        {
            ServerDataHeader header = new ServerDataHeader
            {
                m_nRecordNum = br.ReadInt32(),
                m_nFieldNum = br.ReadInt32(),
                m_nRecordSize = br.ReadInt32()
            };
            return header;
        }

        public object GetLoadedFileData(string fileName)
        {
            switch (Path.GetFileNameWithoutExtension(fileName).ToLower())
            {
                //server files
                case Definitions.STORE_LIST:
                    return serverStore;

                //client files
                //case Definitions.STORE:
                //    return clientStore;

                default:
                    return null;
            }
        }

        public void Load(string fileName)
        {
            BinaryReader br = new BinaryReader(File.OpenRead(fileName));
            int length = (int)br.BaseStream.Length;
            if (length > 0)
            {
                switch (Path.GetFileNameWithoutExtension(fileName).ToLower())
                {
                    //server files
                    case Definitions.STORE_LIST:
                        serverStore = (ServerStore)LoadFile(br, serverStore);
                        break;


                    //client files
                    //case Definitions.STORE:
                    //    clientStore = (ClientStore)LoadFile(br, clientStore);
                    //    break;
                }
            }
            br.Close();
        }

        private object LoadFile(BinaryReader br, object o)
        {
            int headerItemCount = 0;
            Type type = o.GetType();
            FieldInfo[] fields = type.GetFields();

            foreach (FieldInfo field in fields)
            {
                Type fieldType = field.FieldType;
                if (fieldType == typeof(ServerDataHeader))
                {
                    ServerDataHeader serverHeader = ReadServerDataHeader(br);
                    field.SetValue(o, serverHeader);
                    headerItemCount = serverHeader.m_nRecordNum;
                }
                else if (fieldType == typeof(ClientDataHeader))
                {
                    ClientDataHeader clientHeader = ReadClientDataHeader(br);
                    field.SetValue(o, clientHeader);
                    headerItemCount = Convert.ToInt32(clientHeader.nBlocks);
                }
                else
                {
                    // init items array
                    Array items = Array.CreateInstance(fieldType.GetElementType(), headerItemCount);

                    // set array values
                    for (var i = 0; i < headerItemCount; i++)
                    {
                        var x = Marshal.SizeOf(fieldType.GetElementType());
                        byte[] bytes = br.ReadBytes(Marshal.SizeOf(fieldType.GetElementType()));

                        GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);

                        items.SetValue(Marshal.PtrToStructure(handle.AddrOfPinnedObject(), fieldType.GetElementType()), i);
                        handle.Free();
                    }
                    field.SetValue(o, items);
                }
            }
            return o;
        }

        public void Save(string fileName)
        {
            BinaryWriter binaryWriter = new BinaryWriter(File.OpenWrite(fileName));

            switch (Path.GetFileNameWithoutExtension(fileName).ToLower())
            {
                //server files
                case Definitions.STORE_LIST:
                    SaveFile(binaryWriter, serverStore);
                    break;

                //client files
                //case Definitions.STORE:
                //    SaveFile(binaryWriter, clientStore);
                //    break;
            }
        }

        private void SaveFile(BinaryWriter bw, object o)
        {
            Type type = o.GetType();
            FieldInfo[] fields = type.GetFields();
            Array data = new object[0];
            byte[] headerBuffer = new byte[0];
            byte[] dataBuffer = new byte[0];
            int fieldSize = 0;
            int fieldCount = 0;

            bool hasServerHeader = false;
            bool hasClientHeader = false;

            ServerDataHeader serverDataHeader = new ServerDataHeader();
            ClientDataHeader clientDataHeader = new ClientDataHeader();

            foreach (FieldInfo field in fields)
            {
                Type fieldType = field.FieldType;

                if (fieldType == typeof(ServerDataHeader))
                {
                    serverDataHeader = (ServerDataHeader)field.GetValue(o);
                    headerBuffer = new byte[Marshal.SizeOf(typeof(ServerDataHeader))];
                    hasServerHeader = true;
                }
                else if (fieldType == typeof(ClientDataHeader))
                {
                    clientDataHeader = (ClientDataHeader)field.GetValue(o);
                    headerBuffer = new byte[Marshal.SizeOf(typeof(ClientDataHeader))];
                    hasClientHeader = true;
                }
                else
                {
                    if (fieldType.IsArray)
                    {
                        fieldSize = Marshal.SizeOf(fieldType.GetElementType());
                        data = field.GetValue(o) as Array;
                        fieldCount = data.Length;
                        dataBuffer = new byte[Marshal.SizeOf(fieldType.GetElementType())];
                    }
                }
            }

            GCHandle headerHandle = GCHandle.Alloc(headerBuffer, GCHandleType.Pinned);
            if (hasServerHeader)
            {
                serverDataHeader.m_nRecordNum = fieldCount;
                serverDataHeader.m_nRecordSize = fieldSize;
                Marshal.StructureToPtr(serverDataHeader, headerHandle.AddrOfPinnedObject(), true);
            }
            if (hasClientHeader)
            {
                clientDataHeader.nBlocks = Convert.ToUInt32(fieldCount);
                Marshal.StructureToPtr(clientDataHeader, headerHandle.AddrOfPinnedObject(), true);
            }
            headerHandle.Free();
            bw.Write(headerBuffer, 0, headerBuffer.Length);

            for (var i = 0; i < fieldCount; i++)
            {
                GCHandle handle = GCHandle.Alloc(dataBuffer, GCHandleType.Pinned);
                Marshal.StructureToPtr(data.GetValue(i), handle.AddrOfPinnedObject(), true);
                bw.Write(dataBuffer, 0, dataBuffer.Length);
                handle.Free();
            }

            bw.Flush();
            bw.Close();
        }
    }
}
