﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Serialization;

namespace ParserLibrary.Structs
{   
    [StructLayout(LayoutKind.Sequential)]
    public class ClientAccessory : ClientEquipment
    {
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 4)]
        public ushort[] wResist;

        // c++ just magically skips these in data
        [XmlIgnore]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] skip4;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct AccessoryItemClient
    {
        [MarshalAs(UnmanagedType.Struct)]
        public ClientDataHeader header;
        public ClientAccessory[] clientAccessory;
    }
}
