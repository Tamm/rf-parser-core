﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Serialization;

namespace ParserLibrary.Structs
{   
    [StructLayout(LayoutKind.Sequential)]
    public class ClientWeapon : ClientEquipment
    {
        [MarshalAs(UnmanagedType.I4)]
        public int nType;

        [MarshalAs(UnmanagedType.R4)]
        public float fDistance;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwMinStrikingPower;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwMaxStrikingPower;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwMinForcePower;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwMaxForcePower;

        [MarshalAs(UnmanagedType.U1)]
        public byte byAttributeType;

        // c++ just magically skips these in data
        [XmlIgnore]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] skip4;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct WeaponItemClient
    {
        [MarshalAs(UnmanagedType.Struct)]
        public ClientDataHeader header;
        public ClientWeapon[] clientWeapon;
    }
}
