﻿using System.Runtime.InteropServices;

namespace ParserLibrary.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EquipItemFld
    {
        [MarshalAs(UnmanagedType.I4)]
        int m_bExist;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        char[] m_strModle;

        [MarshalAs(UnmanagedType.I4)]
        int m_nIconIDX;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        char[] m_strName;

        [MarshalAs(UnmanagedType.I4)]
        int m_nKindClt;

        [MarshalAs(UnmanagedType.I4)]
        int m_nItemGrade;

        [MarshalAs(UnmanagedType.I4)]
        int m_nFixPart;

        [MarshalAs(UnmanagedType.I4)]
        int m_nHelmetClass;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        char[] m_strCivil;

        [MarshalAs(UnmanagedType.I4)]
        int m_nLevelLim;

        [MarshalAs(UnmanagedType.I4)]
        int m_nExpertID1;

        [MarshalAs(UnmanagedType.I4)]
        int m_nExpertLim1;

        [MarshalAs(UnmanagedType.I4)]
        int m_nExpertID2;

        [MarshalAs(UnmanagedType.I4)]
        int m_nExpertLim2;

        [MarshalAs(UnmanagedType.I4)]
        int m_nMoney;

        [MarshalAs(UnmanagedType.I4)]
        int m_nStdPrice;

        [MarshalAs(UnmanagedType.I4)]
        int m_nStoragePrice;

        [MarshalAs(UnmanagedType.I4)]
        int m_bAbr;

        [MarshalAs(UnmanagedType.I4)]
        int m_nDurUnit;

        [MarshalAs(UnmanagedType.R4)]
        float m_fEquipSpeed;

        [MarshalAs(UnmanagedType.I4)]
        int m_bRepair;

        [MarshalAs(UnmanagedType.I4)]
        int m_nRepPrice;

        [MarshalAs(UnmanagedType.I4)]
        int m_nEffState;

        [MarshalAs(UnmanagedType.I4)]
        int m_nGASpd;

        [MarshalAs(UnmanagedType.I4)]
        int m_nProperty;

        [MarshalAs(UnmanagedType.R4)]
        float m_fFireTol;

        [MarshalAs(UnmanagedType.R4)]
        float m_fWaterTol;

        [MarshalAs(UnmanagedType.R4)]
        float m_fSoilTol;

        [MarshalAs(UnmanagedType.R4)]
        float m_fWindTol;

        [MarshalAs(UnmanagedType.R4)]
        float m_fDefFc;

        [MarshalAs(UnmanagedType.I4)]
        int m_nDefence_DP;

        [MarshalAs(UnmanagedType.I4)]
        int m_nMaxDP;

        [MarshalAs(UnmanagedType.I4)]
        int m_nUpLevelLim;

        [MarshalAs(UnmanagedType.I4)]
        int m_nClassGradeLim;

        [MarshalAs(UnmanagedType.I4)]
        int m_nEff1Code;

        [MarshalAs(UnmanagedType.R4)]
        float m_fEff1Unit;

        [MarshalAs(UnmanagedType.I4)]
        int m_nEff2Code;

        [MarshalAs(UnmanagedType.R4)]
        float m_fEff2Unit;

        [MarshalAs(UnmanagedType.I4)]
        int m_nEff3Code;

        [MarshalAs(UnmanagedType.R4)]
        float m_fEff3Unit;

        [MarshalAs(UnmanagedType.I4)]
        int m_nEff4Code;

        [MarshalAs(UnmanagedType.R4)]
        float m_fEff4Unit;

        [MarshalAs(UnmanagedType.I4)]
        int m_nDuration;

        [MarshalAs(UnmanagedType.I4)]
        int m_bSell;

        [MarshalAs(UnmanagedType.I4)]
        int m_bExchange;

        [MarshalAs(UnmanagedType.I4)]
        int m_bGround;

        [MarshalAs(UnmanagedType.I4)]
        int m_bStoragePossible;

        [MarshalAs(UnmanagedType.I4)]
        int m_bUseableNormalAcc;

        [MarshalAs(UnmanagedType.I4)]
        int m_nUpgrade;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        char[] m_strTooltipIndex;

        [MarshalAs(UnmanagedType.I4)]
        int m_nDefEffType;
    }

    public struct EquipmentItem
    {
        public ServerDataHeader header;
        public EquipItemFld[] equipItemFld;

        public EquipmentItem(ServerDataHeader h, EquipItemFld[] e)
        {
            header = h;
            equipItemFld = e;
        }
    }
}
