﻿using ParserLibrary.Common;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Serialization;

namespace ParserLibrary.Structs
{   
    [StructLayout(LayoutKind.Sequential)]
    public class ClientEquipment : ClientItem
    {
        [MarshalAs(UnmanagedType.I4)]
        public int bIsRepairable;

        [MarshalAs(UnmanagedType.U1)]
        public byte byUpgradeSocketNum;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwRepairPrice;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwMaxDP;

        [MarshalAs(UnmanagedType.U1)]
        public byte byUsableLevel;


        [MarshalAs(UnmanagedType.U1)]
        public byte byUsableExpertnessType1;

        [MarshalAs(UnmanagedType.U1)]
        public byte byUsableExpertnessType2;


        // c++ just magically skips these in data
        [XmlIgnore]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public byte[] skip3;


        [MarshalAs(UnmanagedType.U1)]
        public byte byUsableExpertness1;

        [MarshalAs(UnmanagedType.U1)]
        public byte byUsableExpertness2;


        [MarshalAs(UnmanagedType.I4)]
        public int nSpeed;

        [MarshalAs(UnmanagedType.U1)]
        public byte byMaxEffectNum;


        [MarshalAs(UnmanagedType.U1)]
        public byte byEffectType1;

        [MarshalAs(UnmanagedType.U1)]
        public byte byEffectType2;

        [MarshalAs(UnmanagedType.U1)]
        public byte byEffectType3;

        [MarshalAs(UnmanagedType.U1)]
        public byte byEffectType4;


        [MarshalAs(UnmanagedType.R4)]
        public float fEffectValue1;

        [MarshalAs(UnmanagedType.R4)]
        public float fEffectValue2;

        [MarshalAs(UnmanagedType.R4)]
        public float fEffectValue3;

        [MarshalAs(UnmanagedType.R4)]
        public float fEffectValue4;


        [MarshalAs(UnmanagedType.U4)]
        public uint dwEffectMaterial;

        public string GetEffectString
        {
            get
            {
                string value = "";

                if (fEffectValue1 != 0)
                {
                    value += Definitions.GetEquipSpecialEffectText(byEffectType1) + " " + fEffectValue1;
                }
                if (fEffectValue2 != 0)
                {
                    value += ", " + Definitions.GetEquipSpecialEffectText(byEffectType2) + " " + fEffectValue2;
                }
                if (fEffectValue3 != 0)
                {
                    value += ", " + Definitions.GetEquipSpecialEffectText(byEffectType3) + " " + fEffectValue3;
                }
                if (fEffectValue4 != 0)
                {
                    value += ", " + Definitions.GetEquipSpecialEffectText(byEffectType4) + " " + fEffectValue4;
                }

                return value != "" ? "[" + value + "]" : "";
            }
        }
    }
}
