﻿using System.Runtime.InteropServices;

namespace ParserLibrary.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct StoreItemIds
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] itemId;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct StoreListEntry
    {
        [MarshalAs(UnmanagedType.I4)]
        public int m_dwIndex;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] m_strCode;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] m_strStore_NPCcode;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] m_strStore_NPCname;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] m_strStore_MAPcode;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nStore_trade;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nStore_NPCangle;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nNpc_Class1;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nNpc_Class2;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nNpc_Class3;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nNpc_Class4;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nNpc_Class5;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nStore_LISTcount;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 240)]
        public StoreItemIds[] m_strItemlist;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServerStore
    {
        [MarshalAs(UnmanagedType.Struct)]
        public ServerDataHeader header;
        public StoreListEntry[] storeListEntry;
    }
}
