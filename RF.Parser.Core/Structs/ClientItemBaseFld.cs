﻿using ParserLibrary.Common;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Serialization;

namespace ParserLibrary.Structs
{   
    [StructLayout(LayoutKind.Sequential)]
    public class ClientItem
    {
        private static readonly Encoding koreanEncoding = Encoding.GetEncoding("EUC-KR");

        [MarshalAs(UnmanagedType.U4)]
        public uint dwDTIndex;

        [XmlIgnore]
        [MarshalAs(UnmanagedType.U4)]
        public uint dwDTCode;

        public string DTCode
        {
            get => dwDTCode.ToString("X");
            set
            {
                int.Parse(value, System.Globalization.NumberStyles.HexNumber);
            }
        }

        [XmlIgnore]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] pName;

        public string Name
        {
            get => koreanEncoding.GetString(pName).TrimEnd('\0');
            set
            {
                pName = Encoding.ASCII.GetBytes(value);
                Array.Resize(ref pName, 32);
            }
        }

        [XmlIgnore]
        [MarshalAs(UnmanagedType.U4)]
        public uint dwMeshID;

        public string MeshId
        {
            get => dwMeshID.ToString("X");
            set
            {
                int.Parse(value, System.Globalization.NumberStyles.HexNumber);
            }
        }

        public uint SubTypeCode
        {
            get
            {
                return (dwMeshID & 0x0000FF00) >> 8;
            }
            set
            {

            }
        }

        public string SubTypeText
        {
            get
            {
                switch ((dwMeshID & 0x000F0000) >> 16)
                {
                    case 0: //ITEM_TYPE_NAME_ARMOR
                        return "";
                    case 1: //ITEM_TYPE_NAME_WEAPON
                        return Definitions.weaponSubTypes[SubTypeCode];
                    case 2: //ITEM_TYPE_NAME_CONSUMPTION
                        return "";
                    case 3: //ITEM_TYPE_NAME_ETC
                        return "";
                    case 4: //ITEM_TYPE_NAME_FORCE
                        return "";
                    default:
                        return "";
                }
            }
            set
            {

            }
        }

        [MarshalAs(UnmanagedType.U4)]
        public uint dwIconID;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] byUsableRace;

        [MarshalAs(UnmanagedType.U1)]
        public byte byEquipType;

        [MarshalAs(UnmanagedType.U1)]
        public byte byMoneyType;

        // c++ just magically skips these in data
        [XmlIgnore]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public byte[] skip1;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwStandardPrice;

        [MarshalAs(UnmanagedType.I4)]
        public int bIsExchangable;

        [MarshalAs(UnmanagedType.I4)]
        public int bIsSellable;

        [MarshalAs(UnmanagedType.I4)]
        public int bIsDumpable;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwDescriptionDTIndex;
    }
}
