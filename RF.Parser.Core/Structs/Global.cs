﻿using System.Runtime.InteropServices;

namespace ParserLibrary.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ClientDataHeader
    {
        [MarshalAs(UnmanagedType.U4)]
        public uint nBlocks;

        [MarshalAs(UnmanagedType.U4)]
        public uint nColumns;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServerDataHeader
    {
        [MarshalAs(UnmanagedType.I4)]
        public int m_nRecordNum;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nFieldNum;

        [MarshalAs(UnmanagedType.I4)]
        public int m_nRecordSize;
    }
}
