﻿using System.Runtime.InteropServices;

namespace ParserLibrary.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct StoreItems
    {
        [MarshalAs(UnmanagedType.U1)]
        public byte byItemType;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwDTCode;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct StoreEntry
    {
        [MarshalAs(UnmanagedType.U4)]
        public uint dwIndex;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwDTCode;

        [MarshalAs(UnmanagedType.U1)]
        public byte byRace;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] pName;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] pDummyName;

        [MarshalAs(UnmanagedType.U1)]
        public byte byMerchantType;

        [MarshalAs(UnmanagedType.R4)]
        public float fYAngle;

        [MarshalAs(UnmanagedType.U4)]
        public uint dwMaxItemNum;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 240)]
        public StoreItems[] pItemList;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
        public byte[] pButtonFunction;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
        public byte[] pDescription;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ClientStore
    {
        [MarshalAs(UnmanagedType.Struct)]
        public ClientDataHeader header;
        public StoreEntry[] storeEntry;
    }
}
