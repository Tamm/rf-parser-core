﻿using System.Collections.Generic;

namespace ParserLibrary.Common
{
    class Definitions
    {
        //Server side dat files
        public const string STORE_LIST = "storelist";

        public const string HELMET_ITEM = "helmetitem";
        public const string UPPER_ITEM = "upperitem";
        public const string LOWER_ITEM = "loweritem";
        public const string GAUNTLET_ITEM = "gauntletitem";
        public const string SHOE_ITEM = "shoeitem";

        //Client side dat files
        public const string STORE = "store";

        public const string HELMET_ITEM_CLT = "helmet";
        public const string UPPER_ITEM_CLT = "upper";
        public const string LOWER_ITEM_CLT = "lower";
        public const string GAUNTLET_ITEM_CLT = "gloves";
        public const string SHOE_ITEM_CLT = "shoes";
        public const string SHIELD_ITEM_CLT = "shield";
        public const string AMULET_ITEM_CLT = "amulet";
        public const string RING_ITEM_CLT = "ring";
        public const string WEAPON_ITEM_CLT = "weapon";

        public static Dictionary<byte, string> itemsTypes = new Dictionary<byte, string>
        {
            {0,"None"},
            {1,"Upper"},
            {2,"Lower"},
            {3,"Gloves"},
            {4,"Boots"},
            {5,"Helmet"},
            {6,"Weapon"},
            {7,"Shield"},
            {8,"Cloaks"},
            {9,"Ring"},
            {10,"Amulets"},
            {11,"Ammo"},
            {12,"MakeTool"},
            {13,"Potion"},
            {14,"Bag"},
            {15,"Battery"},
            {16,"Ore"},
            {17,"Resource"},
            {18,"Force"},
            {19,"UnitKey"},
            {20,"Booty"},
            {21,"Map"},
            {22,"Scrolls"},
            {23,"BattleDungeon"},
            {24,"Animus"},
            {25,"Towers"},
            {26,"Traps"},
            {27,"SeigeKits"},
            {28,"Ticket"},
            {29,"Event"},
            {30,"Recovery"},
            {31,"Box"},
            {32,"FireCracker"},
            {33,"UnMannedMiner"},
            {34,"Radar"},
            {35,"Pagers"},
            {36,"Coupons"},
            {37,"UnitHead"},
            {38,"UnitUpper"},
            {39,"UnitLower"},
            {40,"UnitArms"},
            {41,"UnitShoulder"},
            {42,"UnitBack"},
            {43,"UnitBullet"}
        };

        enum ITEM_SPECIAL_EFFECT_TYPE
        {
            ISET_NONE, ISET_INC_SP, ISET_DEC_USING_FP, ISET_INC_HIT_PROB, ISET_INC_AVOID_PROB,
            ISET_INC_MAX_HPFP, ISET_INC_STRIKING_POWER, ISET_INC_DEFENCE_POWER, ISET_INC_SKILL_LEVEL, ISET_ENABLE_STEALTH,
            ISET_ENABLE_DETECT, ISET_CANCEL_LOSS_SUPPORT_SKILL, ISET_INC_SPEED, ISET_ENABLE_MARK_WEEK_POINT
        };

        public static string GetEquipSpecialEffectText(int byEffectType)
        {
            switch (byEffectType)
            {
                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_NONE:
                    return "";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_INC_SP:
                    return "SP";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_DEC_USING_FP:
                    return "FP";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_INC_HIT_PROB:
                    return "HIT";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_INC_AVOID_PROB:
                    return "E";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_INC_MAX_HPFP:
                    return "HP";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_INC_STRIKING_POWER:
                    return "A";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_INC_DEFENCE_POWER:
                    return "D";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_INC_SKILL_LEVEL:
                    return "+";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_ENABLE_STEALTH:
                    return "H";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_ENABLE_DETECT:
                    return "R";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_CANCEL_LOSS_SUPPORT_SKILL:
                    return "L";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_INC_SPEED:
                    return "V";

                case (int)ITEM_SPECIAL_EFFECT_TYPE.ISET_ENABLE_MARK_WEEK_POINT:
                    return "W";
                default:
                    return "";
            }
        }

        public static Dictionary<uint, string> weaponTypes = new Dictionary<uint, string>
        {
            {0,"Knife"},
            {1,"Sword"},
            {2,"Dual sword"},
            {3,"Axe"},
            {4,"Mace"},
            {5,"Spear"},
            {6,"Bow"},
            {7,"Firearm"},
            {8,"Throwing"},
            {9,"Staff"},
            {10,"Mineing"},
        };

        public static Dictionary<uint, string> weaponSubTypes = new Dictionary<uint, string>
        {
            {0,"Sword RH"},
            {1,"Sword 2H"},
            {2,"Dual sword"},
            {3,"Knife R"},
            {4,"Knife 2H"},
            {5,"Knife throw LH"},
            {6,"Axe RH"},
            {7,"Axe 2H"},
            {8,"Axe throw LH"},
            {9,"Dual axe"},
            {10,"Mace RH"},
            {11,"Mace 2H"},
            {12,"Staff RH"},
            {13,"Staff 2H"},
            {14,"Spear 2H"},
            {15,"Bow"},
            {16,"Crossbow"},
            {17,"Gun"},
            {18,"Dual gun"},
            {19,"Rifle"},
            {20,"Launcher"},
            {21,"Faust"},
            {22,"Machinegun"},
            {23,"Flamethrower"},
            {24,"Beam gun"},
            {25,"Dual beam gun"},
            {26,"Beam rifle"},
            {27,"Plasma gun"},
            {28,"Mining tool"}
        };
    }
}
